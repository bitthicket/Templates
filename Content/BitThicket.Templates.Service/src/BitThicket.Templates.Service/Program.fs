﻿namespace BitThicket.Templates.Service

open System
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open BitThicket.Templates.Serivce

module Program =
    let createHostBuilder args =
        Host.CreateDefaultBuilder(args)
            .ConfigureServices(fun services ->
                services.AddHostedService<Service>()
                |> ignore)

    [<EntryPoint>]
    let main args =
        (createHostBuilder args)
            .Build()
            .Run()
        0
