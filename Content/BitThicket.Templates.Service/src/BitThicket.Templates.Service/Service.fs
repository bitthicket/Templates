namespace BitThicket.Templates.Serivce

open System
open Microsoft.Extensions.Hosting
open Microsoft.Extensions.Logging

type Service(logger:ILogger<Service>) =
    inherit BackgroundService()

    override this.ExecuteAsync(cancellationToken) = 
        async {
            while not cancellationToken.IsCancellationRequested do
                logger.LogInformation("Worker running at: {time}", DateTimeOffset.Now)
                do! Async.Sleep 1000 
        }  
        |> Async.StartAsTask 
        :> System.Threading.Tasks.Task