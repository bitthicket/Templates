module Namespace.Program

open Logary
open Logary.Message
open Config

[<EntryPoint>]
let main argv =
    let _log = getLogger "main"
    _log.info (eventX "starting")

    parseAllConfig argv |> ignore

    // do stuff

    0 // return an integer exit code
