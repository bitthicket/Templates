module internal Namespace.Config

open System
open System.IO
open System.Net
open Argu
open Logary
open Logary.Configuration
open Logary.Message
open Logary.Targets


type Arguments =
    | [<CustomCommandLine("--log-level", "-l")>]
      LogLevel of LogLevel
    with
        interface IArgParserTemplate with
            member this.Usage =
                match this with
                | LogLevel _ -> sprintf "control verbosity of output; default [%A]" LogLevel.Debug

let private buildLogary() =
     let console = LiterateConsole.create LiterateConsole.empty "console"
     Config.create "Namespace" (Dns.GetHostName())
     |> Config.targets [console]
     |> Config.ilogger (ILogger.LiterateConsole Info)
     |> Config.buildAndRun

let private logary = buildLogary()
let getLogger name =
    sprintf "%s.%s" "Namespace" name 
    |> PointName.parse 
    |> logary.getLogger

let _log = getLogger "Namespace.Config"

let mutable private parsedArgs : ParseResults<Arguments> option = None
let parseAllConfig args =
    match parsedArgs with
    | Some _ ->
        _log.error (eventX "Attempt to set args after initialization")
        Result.Error()
    | None ->
        let errorHandler = ProcessExiter(colorizer = function 
                                                     | ErrorCode.HelpText -> None
                                                     | _ -> Some ConsoleColor.Red)
        let programName = Path.GetFileName(Environment.GetCommandLineArgs().[0])
        let parser = ArgumentParser.Create<Arguments>(programName = programName, errorHandler = errorHandler)
        let pa = parser.Parse(inputs = args)
        let logLevel = pa.GetResult(LogLevel, Info)

        _log.info (eventX <| sprintf "settings log level: %A " logLevel)
        logary.switchLoggerLevel (".*", logLevel)

        parsedArgs <- Some pa
        Result.Ok()